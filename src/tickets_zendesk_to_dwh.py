# Import the Zenpy Class
from zenpy import Zenpy
import json
from utils.config import Configuration as Config
from utils.postgres_helper import get_connection
from utils.utils import get_global_config

# server configurations
# Connecting DB..
conn = get_connection(get_global_config(), 'pg_dwh')
print("Database connected...")
conn.set_client_encoding('latin-1')
cur = conn.cursor()

# credentials to connect Zendesk API
creds = {
    'email': 'jan.gottschau@sunday.de',
    'token': 'MZb38sakvZ6pK25AFGiW0KuQQyiJhIa40ZKinZvn',
    'subdomain': 'sunday'
}

# connect to Zendesk API using Zenpy wrapper
zenpy_client = Zenpy(**creds)

# get all tickets
tickets = zenpy_client.tickets()

# loop the tickets and insert to dwh
for ticket in tickets:
    ticket_json = json.dumps(ticket.to_dict(), sort_keys=False)
    insert_query = '''INSERT INTO zen_test.tickets(info) VALUES ( $$ ''' + ticket_json + ''' $$ )'''
    cur.execute(insert_query)
    conn.commit()

conn.close()
