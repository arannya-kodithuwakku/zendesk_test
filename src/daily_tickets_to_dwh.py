# Import the Zenpy Class
from zenpy import Zenpy
import datetime
import json
from utils.config import Configuration as Config
from utils.postgres_helper import get_connection
from utils.utils import get_global_config

# server configurations
# Connecting DB..
conn = get_connection(get_global_config(), 'pg_dwh')
print("Database connected...")
conn.set_client_encoding('latin-1')
cur = conn.cursor()

# Zenpy accepts an API token
creds = {
    'email': 'jan.gottschau@sunday.de',
    'token': 'MZb38sakvZ6pK25AFGiW0KuQQyiJhIa40ZKinZvn',
    'subdomain': 'sunday'
}

# Default
zenpy_client = Zenpy(**creds)

# date depend on the machine time
date = (datetime.datetime.utcnow().date() - datetime.timedelta(days=1)).isoformat()

# date for the filter
yesterday_start = date + '00:00:00Z'
yesterday_start = datetime.datetime.strptime(yesterday_start, '%Y-%m-%d%H:%M:%SZ')
yesterday_end = date + '23:59:59Z'
yesterday_end = datetime.datetime.strptime(yesterday_end, '%Y-%m-%d%H:%M:%SZ')

# query to get the response
updated_tickets = zenpy_client.search(type='ticket', updated_at_between=[yesterday_start, yesterday_end])

# save to dwh
for ticket in updated_tickets:
    ticket_json = json.dumps(ticket.to_dict(), sort_keys=False)
    insert_query = '''INSERT INTO zen_test.tickets(info) VALUES ( $$ ''' + ticket_json + ''' $$ )'''
    cursor.execute(insert_query)
    connection.commit()

